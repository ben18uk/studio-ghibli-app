# studio-ghibli-app

> An application to display and sort Studio Ghibli films.

> This project was created using the vue-cli.

> It uses Vue.js with ES6 class style notation. I have also decided to break .vue files into .html, .css and .js to increase readability.

> I have also experimented with Vuex to create a rudimentary store for state management (used to pass film details to the modal component).

> 19/04/2018: I have changed the table-layout to a card-layout to better show my CSS capabilities. The film-card component includes CSS that could be recreated easier using bootstrap classes, but I have uses CSS where possible as a demonstration.

## Build Setup

``` bash
### install dependencies
npm install

### Uses concurrently to start the server on port 8080 and the frontend on port 3000.
npm run start

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

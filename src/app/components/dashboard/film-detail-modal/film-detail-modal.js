import Vue from "vue";
import Component from "vue-class-component";

@Component({})
export default class FilmDetailModal extends Vue {
    get film() {
        return this.$store.getters.getFilm;
    };
};

import DashboardHeader from "./dashboard-header/dashboard-header.vue";
import FilmRow from "./film-row/film-row.vue";
import FilmCard from "./film-card/film-card.vue";
import FilmDetailModal from "./film-detail-modal/film-detail-modal.vue";
import FilmsSort from "./films-sort/films-sort.vue";
import Vue from "vue";
import Component from "vue-class-component";

@Component({
    components: {
        DashboardHeader,
        FilmRow,
        FilmCard,
        FilmDetailModal,
        FilmsSort
    }
})
export default class Dashboard extends Vue {
    get films() {
        return this.$store.getters.getFilms;
    };

    // On mount, get films data from API and set store
    mounted() {
        const apiPathUrl = "http://localhost:8080";

        fetch(apiPathUrl + "/films")
            .then((res) => {
                if (res.status !== 200) {
                    console.log("Error:" + res.status);
                    return;
                }
                res.json().then((data) => {
                    // Set films to store
                    this.$store.dispatch("setFilms", {
                        films: data
                    });
                });
            }
            )
            .catch(function(err) {
                console.log("Error: ", err);
            });
    }
};

import Vue from "vue";
import Component from "vue-class-component";

@Component({
    props: ["film"]
})
export default class FilmRow extends Vue {
    setFilmToStore() {
        this.$store.dispatch("setFilm", {
            film: this.film
        });
    }
};

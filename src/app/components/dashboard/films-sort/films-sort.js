/* eslint-disable indent */
import Vue from "vue";
import Component from "vue-class-component";
import * as _ from "lodash";

@Component({})
export default class FilmsSort extends Vue {
    get films() {
        return this.$store.getters.getFilms;
    };

    sort = "Release Date";
    order = "Ascending";

    sortTable(sort, order) {
        switch (sort) {
            case "title":
                this.sort = "Title";
                break;
            case "release_date":
                this.sort = "Release Date";
                break;
            case "rt_score":
                this.sort = "Rotten Tomatoes Score";
        }

        // Changes strings to numbers, then sorts
        const newFilmsList = _.orderBy(this.films, (item) => {
            const value = item[sort];
            if (!isNaN(value)) {
                return +value;
            }
            return value;
        });

        // Handle order
        if (order === null) {
            this.order = "Ascending";
        } else if (order !== this.order) {
            this.order = order;
            newFilmsList.reverse();
        }

        // set store with new order
        this.$store.dispatch("setFilms", {
            films: newFilmsList
        });
    }
};

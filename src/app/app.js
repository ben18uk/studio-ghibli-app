import Dashboard from "./components/dashboard/dashboard.vue";
import Vue from "vue";
import Component from "vue-class-component";

@Component({
    components: {
        Dashboard
    }
})
export default class App extends Vue {

};

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "es6-promise/auto";
import "isomorphic-fetch";

import Vue from "vue";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import Vuex from "vuex";

Vue.use(BootstrapVue);
Vue.use(Vuex);

Vue.config.productionTip = false;

// Store (should be defined in its own file in a larger app)
const store = new Vuex.Store({
    state: {
        films: null,
        currentFilm: null
    },
    mutations: {
        setFilm(state, film) {
            state.currentFilm = film;
        },
        setFilms(state, films) {
            state.films = films;
        }
    },
    getters: {
        getFilm: state => {
            return state.currentFilm;
        },
        getFilms: state => {
            return state.films;
        }
    },
    actions: {
        setFilm(context, {film}) {
            context.commit("setFilm", film);
        },
        setFilms(context, {films}) {
            context.commit("setFilms", films);
        }
    }
});

/* eslint-disable no-new */
new Vue({
    el: "#app",
    store: store,
    components: {App},
    template: "<App/>"
});
